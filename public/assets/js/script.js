const canvas = document.getElementById("the_canvas");    //to be able draw on canvas
const context = canvas.getContext("2d");

let gameStatus = new Boolean(true);
const sound = new Audio("assets/music/song.wav");



let direction = 0;      //0 - none, 1- down, 2- left, 3 - right, 4 - up
let frames = 3;         //i've 3 frames in a row for character
let currentFrame = 0;   //for player animation
let playerX = 10;       //position of player (by default)
let playerY = 100;


let starFrames = 7;     //frames for star animation
let currentStarFrame = 0;

let starX = 300;        //position of star Nr1
let starY = 300;

let starTwoX = 600;        //position of star Nr2
let starTwoY = 450;

let starThreeX = 1000;        //position of star Nr3
let starThreeY = 100;


let asteroidFrames = 2;    //frames for asteroid animation
let currentAsteroidFrame = 0;

let asteroidX = 350;       //position of donut
let asteroidY = 350;

let asteroidTwoX = 400;       //position of donut
let asteroidTwoY = 50;

let asteroidThreeX = 1000;       //position of donut
let asteroidThreeY = 200;


let scoreCounter = 0;  //player score counter
const speed = 5;


//------------------------------CHARACTER---------------------------------------//
function GamerInput(input)
{
    this.action = input;
}

let gamerInput = new GamerInput("None");


function GameObjects(spritesheet, x, y, width, height)
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let sprite = new Image();   //it makes java script aware that it is image object, set image
sprite.src = "assets/img/Character_Sprites.png"; //image location
let player = new GameObjects(sprite, playerX, playerY, 60, 40);

let starSprite = new Image();
starSprite.src = "assets/img/star.png";
let star = new GameObjects(starSprite, starX, starY, 11, 10);
let starTwo = new GameObjects(starSprite, starTwoX, starTwoY, 11, 10);
let starThree = new GameObjects(starSprite, starThreeX, starThreeY, 11, 10);

let asteroidSprite = new Image();
asteroidSprite.src = "assets/img/asteroid.png";
let asteroid = new GameObjects(asteroidSprite, asteroidX, asteroidY, 77, 35);
let asteroidTwo = new GameObjects(asteroidSprite, asteroidTwoX, asteroidTwoY, 77, 35);
let asteroidThree = new GameObjects(asteroidSprite, asteroidThreeX, asteroidThreeY, 77, 35);

function draw()
{
    //drawBackground(time);
    animate();
    drawTimeBar();
    drawHealthbar();

    if(gameStatus)
    {
        collision();
    }

    displayScore();

    if (fillVal >= 0.99)    //to cover all gameplay and clock
    {
        gameOver();
    }

    if (fillValue >= 0.99)    //to cover all gameplay and clock
    {
        gameOver();
    }
}


/*
time = new Date().getTime();
function drawBackground(time)
{
    distance += calculateOffset(time);
    if(distance > loopingBackground.width)
    {
        distance = 0;
    }

    context.translate(distance, 0);
    context.drawImage(loopingBackground,0,0);
    context.drawImage(loopingBackground,-loopingBackground.width + 1, 0);

    //requestAnimationFrame(draw);
}
*/


let initial = new Date().getTime(); 
console.log(initial);
let current;

function animate()
{
    current = new Date().getTime();
    if (current - initial >= 200)  //how much time i want to collapse frames, every 200 milisec, basically, we allow frame increments to change
    {
        currentFrame = (currentFrame + 1) % frames;     //changing current frame
        currentStarFrame = (currentStarFrame + 1) % starFrames;
        currentAsteroidFrame = (currentAsteroidFrame + 1) % asteroidFrames;

        initial = current;      //reset initial
    }

    context.clearRect(0, 0, canvas.width, canvas.height); //that it won't be visible on the screen all layers of player

    context.drawImage(sprite, (sprite.width / frames) * currentFrame, direction * 64, 120, 64, player.x, player.y, 120, 64);  //want to draw an image on canvas, position by deafult  

    context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 22, 20, star.x, star.y, 22, 20);   //draw star Nr1
    context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 22, 20, starTwo.x, starTwo.y, 22, 20);   //draw star Nr2
    context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 22, 20, starThree.x, starThree.y, 22, 20);   //draw star Nr3

    context.drawImage(asteroidSprite, (asteroidSprite.width / asteroidFrames) * currentAsteroidFrame, 0, 77, 35, asteroid.x, asteroid.y, 77, 35);   //draw asteroid Nr1
    context.drawImage(asteroidSprite, (asteroidSprite.width / asteroidFrames) * currentAsteroidFrame, 0, 77, 35, asteroidTwo.x, asteroidTwo.y, 77, 35); //draw asteroid Nr2
    context.drawImage(asteroidSprite, (asteroidSprite.width / asteroidFrames) * currentAsteroidFrame, 0, 77, 35, asteroidThree.x, asteroidThree.y, 77, 35); //draw asteroid Nr3
}


function input(event) //player interaction with (keyboard) character (event listener)
{
    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            

            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            

            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key

            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key

            default:
                gamerInput = new GamerInput("None");    //if player is not interacting
        }
    }

    else
    {
        gamerInput = new GamerInput("None");
    }

}

function update()   //to make character moving
{
    if(gamerInput.action === "None")
    {
        direction = 0;
    }

    if (player.y <= 0)          /////  UP  /////
    {
        //do nothing
    }

    else 
    {
        if (gamerInput.action === "Up")
        {
            direction = 4;
            player.y -= speed;
        }
    }

    
    if (player.y >= 415)        /////  DOWN  /////
    {
        //do nothing
    }

    else
    {
        if (gamerInput.action === "Down")
        {            
            direction = 1;
            player.y += speed;   
        }
    }

  
    if (player.x <= 0)          /////  LEFT  /////
    {
        //do nothing
    }

    else
    {
        if (gamerInput.action === "Left")
        {
            direction = 2;              
            player.x -= speed;
        }
    }


    if (player.x >= 1035)          /////  RIGHT  /////
    {
        //do nothing
    }

    else
    {
        if (gamerInput.action === "Right")
        {
            direction = 3;
            player.x += speed;
        }
    }
}

//------------------------------------RANDOM POSITION-----------------------------------------//

let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);


function randoPos(rangeX, rangeY, delta){
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}


function asteroidPos()
{
    asteroid.x = Math.floor(Math.random() * 1050); 
    asteroid.y = Math.floor((Math.random() * 340) + 100); 
}

function asteroidTwoPos()
{
    asteroidTwo.x = Math.floor(Math.random() * 1050); 
    asteroidTwo.y = Math.floor((Math.random() * 340) + 100); 
}

function asteroidThreePos()
{
    asteroidThree.x = Math.floor(Math.random() * 1050); 
    asteroidThree.y = Math.floor((Math.random() * 340) + 100); 
}


let counter = 0;
let seconds = 0;
let secondsTwo = 0;
let secondsThree = 0;

function collision()  //put in draw function
{
    counter++;

    if (counter >= 143)
    {
        seconds++;
        secondsTwo++;
        secondsThree++;
        counter = 0;
    }

    if(secondsTwo >= 3)
    {
        secondsTwo = 0;
        asteroidTwoPos();
    }

    if(secondsThree >= 2)
    {
        secondsThree = 0;
        asteroidThreePos();
    }

    if(seconds >= 4)
    {
        seconds = 0;
        asteroidPos();
    }
    
    
    if (player.x < asteroid.x + asteroid.width && player.x + player.width > asteroid.x && player.y < asteroid.y && player.y + player.height > asteroid.y ) 
    {
        context.drawImage(asteroidSprite, (asteroidSprite.width / asteroidFrames) * currentAsteroidFrame, 0, 77, 35, asteroid.x, asteroid.y, 7, 35);
        console.log("collision!");

        fillVal += 0.34;    //  1/3 part of players healh goes off

        seconds = 0; 
        asteroidPos();
    } 

    if (player.x < asteroidTwo.x + asteroidTwo.width && player.x + player.width > asteroidTwo.x && player.y < asteroidTwo.y && player.y + player.height > asteroidTwo.y ) 
    {
        context.drawImage(asteroidSprite, (asteroidSprite.width / asteroidFrames) * currentAsteroidFrame, 0, 77, 35, asteroidTwo.x, asteroidTwo.y, 7, 35);
        console.log("collision!");

        fillVal += 0.34;    //  1/3 part of players healh goes off

        seconds = 0; 
        asteroidTwoPos();
    } 

    if (player.x < asteroidThree.x + asteroidThree.width && player.x + player.width > asteroidThree.x && player.y < asteroidThree.y && player.y + player.height > asteroidThree.y ) 
    {
        context.drawImage(asteroidSprite, (asteroidSprite.width / asteroidFrames) * currentAsteroidFrame, 0, 77, 35, asteroidThree.x, asteroidThree.y, 7, 35);
        console.log("collision!");

        fillVal += 0.34;    //  1/3 part of players healh goes off

        seconds = 0; 
        asteroidThreePos();
    } 



//player width is 120px and 72 height but because it's flying and moving, there in most of frames will be empty space, so I've to type specific px size

    //if (player.x < star.x + star.width && player.x + player.width > star.x && player.y < star.y && player.y + player.height > star.y )
    if (player.x < star.x + star.width && player.x + 100 > star.x && player.y < star.y && player.y + 50 > star.y )  
    {
        context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 11, 10, star.x, star.y, 22, 20);
        star.x = Math.floor(Math.random() * 1050);
        star.y = Math.floor((Math.random() * 340) + 120);   //140

        scoreCounter = scoreCounter + 5;
    }

    if (player.x < starTwo.x + starTwo.width && player.x + 100 > starTwo.x && player.y < starTwo.y && player.y + 50 > starTwo.y )  
    {
        context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 11, 10, starTwo.x, starTwo.y, 22, 20);
        starTwo.x = Math.floor(Math.random() * 1050);
        starTwo.y = Math.floor((Math.random() * 340) + 120);   //140

        scoreCounter = scoreCounter + 5;
    }

    if (player.x < starThree.x + starThree.width && player.x + 100 > starThree.x && player.y < starThree.y && player.y + 50 > starThree.y )  
    {
        context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 11, 10, starThree.x, starThree.y, 22, 20);
        starThree.x = Math.floor(Math.random() * 1050);
        starThree.y = Math.floor((Math.random() * 340) + 120);   //140

        scoreCounter = scoreCounter + 5;
    }

}

//--------------------------------SCORE TEXT TO DISPLAY ON SCREEN--------------------------------------------//

function displayScore()
{
    context.fillStyle = 'white';    //to make score text white color
    let scoreString = "score: " + scoreCounter;
    console.log("score" + scoreCounter);
    context.font = '22px monospace';
    context.fillText(scoreString, 950, 25);

     //TEXT MESSAGE
     context.fillText("health: ", 5, 20);
}


//--------------------------------the working of a mouse-based joystick-------------------------------------//

    let static = nipplejs.create({
        zone: document.getElementById('joystick'),
        mode: 'static',
        position: {left: '50%', top: '50%'},
        color: 'white'
    });


    //nipple.on('start move end dir plain', function (evt) {
    static.on('dir:up', function (evt, data) 
    {
        gamerInput = new GamerInput("Up");
    });
    static.on('dir:down', function (evt, data) 
    {
        gamerInput = new GamerInput("Down");
    });
    static.on('dir:left', function (evt, data) 
    {
        gamerInput = new GamerInput("Left");
    });
    static.on('dir:right', function (evt, data) 
    {
        gamerInput = new GamerInput("Right");
    });
    static.on('end', function (evt, data)
    {
        gamerInput = new GamerInput("None");
    });


//-----------------------D-PAD BUTTONS--------------------------------------

function noMoving()
{
    gamerInput = new GamerInput("None");
}

function pressUp()
{
    gamerInput = new GamerInput("Up");
}

function pressDown()
{
    gamerInput = new GamerInput("Down");
}

function pressLeft()
{
    gamerInput = new GamerInput("Left");
}

function pressRight()
{
    gamerInput = new GamerInput("Right");
}





//----------------------------LOOPING BACKGROUND-----------------------------------------------
/*
let loopingBackground = new Image();  
loopingBackground.src = "assets/img/space_background.png"; 

var velocity = 100; //100px per second
var distance = 0;
var lastFrameRepaintTime = 0;

function calculateOffset(time)
{
    var frameGapTime = time - lastFrameRepaintTime;
    lastFrameRepaintTime = time;
    var translateX = velocity * (frameGapTime/1000);
    return translateX;
}
*/



//------------------------------------BUTTONS FOR FULLSCREEN MODE------------------------------------------//

var element = document.documentElement; /* Get the element you want displayed in fullscreen */ 

function openFullscreen()   /* Function to open fullscreen mode */
{
  if (element.requestFullscreen) 
  {
    element.requestFullscreen();
  } 
  
  else if (element.mozRequestFullScreen) 
  { /* Firefox */
    element.mozRequestFullScreen();
  } 
  
  else if (element.webkitRequestFullscreen) 
  { /* Chrome, Safari & Opera */
    element.webkitRequestFullscreen();
  } 
  
  else if (element.msRequestFullscreen) 
  { /* IE/Edge */
    element = window.top.document.body; //To break out of frame in IE
    element.msRequestFullscreen();
  }
}


function closeFullscreen()      /* Function to close fullscreen mode */
{
    if (document.exitFullscreen) /* Code examples are taken from w3schools.com */
    {
      document.exitFullscreen();
    } 
    
    else if (document.mozCancelFullScreen) 
    {
      document.mozCancelFullScreen();
    } 
    
    else if (document.webkitExitFullscreen) 
    {
      document.webkitExitFullscreen();
    } 
    
    else if (document.msExitFullscreen) 
    {
      window.top.document.msExitFullscreen();
    }
}

// Events for button
var output = document.getElementById("the_canvas");

document.addEventListener("fullscreenchange", function() 
{
  output.innerHTML = "fullscreenchange event fired!";
});

document.addEventListener("mozfullscreenchange", function() 
{
  output.innerHTML = "mozfullscreenchange event fired!";
});

document.addEventListener("webkitfullscreenchange", function() 
{
  output.innerHTML = "webkitfullscreenchange event fired!";
});

document.addEventListener("msfullscreenchange", function() 
{
  output.innerHTML = "msfullscreenchange event fired!";
});

//------------------------------DRAW TIME BAR-----------------------------------------------------

var fillValue = 0;
function drawTimeBar() 
{
    var width = 1100;
    var height = 20;
  
    // Draw the background
    context.fillStyle = "#000000";
    context.clearRect(0, 480, canvas.width, canvas.height);
    context.fillRect(0, 480, width, height);
  
    // Draw the fill
    context.fillStyle = "#F09EFF";
    context.fillRect(0, 480, fillValue * width, height);

    fillValue += 0.0005;  //timer

    
    if (fillValue >= 0.99)    //when time value is bigger than 0.99, then draws over black screen with message "Game Over"
    {
        gameOver();
    }
}


// Draw a HealthBar on Canvas, can be used to indicate players health
var fillVal = 0;
function drawHealthbar() 
{
    var width = 200;
    var height = 20;
  
    // Draw the background
    context.fillStyle = "white";
    context.clearRect(100, 3, width, height); //erases everything on its way and below
    context.fillRect(100, 3, width, height);
  
    // Draw the fill
    context.fillStyle = "#000000";
    context.fillRect(300, 3, fillVal * -width, height);

    
    if (fillVal >= 0.99)    //when time value is bigger than 0.99, then draws over black screen with message "Game Over"
    {
        gameOver();
    }
}


//------------------------------END OF GAME SCREEN-----------------------------------------------------

function gameOver()
{
    context.fillStyle = '#33123C';
    context.fillRect(0,0,1100,500);     //x,y,width,height of the canvas
    context.font = '120px Courier New';
    context.fillStyle = 'white';
    context.fillText("GAME OVER", 200, 250);    
    context.font = '30px Courier New';
    context.fillText("Your chance of reaching utopia: " + scoreCounter + " %", 200, 400);

    static.destroy();   //to make joystick dissapear after game is over, so player can't move and earn points even after game is finished
    gameStatus = false;
}

//------------------------------------------------------------------------------------------------------


function gameloop()
{
    update();   //process the user inputs and update the position vars
    draw();     //use the updated values and actually draw the images on screen

    window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);


window.addEventListener('keydown', input);
window.addEventListener('keyup', input);

sound.play(); //doesnt play in Chrome sadly(((